# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 11:05:27 2021

@author: boillat

---------------------------------------------------------------------------------

                                    PyFCSim

This file contains a series of functions for the simulation of oxygen transport
in polymer electrolyte fuel cells (PEFCs) and its interplay with proton transport
in the membrane and catalyst layer. The simulated geometry is a 2D representation
of a differential fuel cell. The oxygen concentration is assumed to be homogeneous
in the flow channel.

What is included:
-	O2 transport approximated by binary diffusion (Fick’s law), Knudsen diffusion
    and film diffusion
-	2D geometry with GDL, MPL and CL
-	Proton transport in the membrane and CL
-	Tafel approximation for the kinetics, with fixed parameters
    (future implementation may include potential dependent parameters)
What is not included:
-	Multicomponent diffusion (e.g. to take into account H2O vapor) and convective
    transport
-	Any kind of H2O transport (the membrane and CL conductivity have to be 
    entered as parameters)
-	The impact of liquid water
-	Any effects on the anode side
-	Dynamics (EIS simulation planned in a future implementation)


The most important high level functions are:
    - get_iv_2d: simulate a single IV curve based on a set of parameters
    - sim_pga: simulate a pulsed gas analysis (PGA) experiment
    - sim_lca: simulate a limiting current analysis (LCA) experiment
    
the list of simulation parameters and their description is given in the
"def_params" definition below.
    
(c) 2021 Paul Scherrer Institut (PSI)
---------------------------------------------------------------------------------

"""

import numpy as np

# ---------------------------------------------------------------------------------

    # default parameters
def_params = {
    'physics':{
        'R':8.3145,
        'F':96485,
        'z':4,
        'D_O2_N2_1bar':0.3e-4,  # O2 diffusivity in N2 at 1 bar [m^2/s]
        'D_O2_He_1bar':1e-4,    # O2 diffusivity in He at 1 bar [m^2/s]
        'c0':44.6               # O2 concentration in normal conditions (1.01325 bar, 0°C) [mol/m^3]
        },
    'conditions':{
        'p': 2.3e5,             # Total pressure in [N/m^2]
        'T': 353.15,            # Temperature in [K]
        'carrier': 'N2',        # Carrier gas
        'RH':1.0,               # relative humidity [unitless]
        'x_O2': 0.208,          # O2 molar fraction in dry gas [unitless]
        'alpha': 1.0,           # transfer coefficient [unitless]
        'gamma': 1.0,           # order of reaction [unitless]
        'heta_min': 0.2,        # minimum overpotential [V]
        'heta_max': 1.0         # maximum overpotential [V]
        },
    'geometry':{
        'rib_width': 250e-6,    # rib width [m]
        'chan_width': 600e-6,   # channel width [m]
        'GDL_thick': 125e-6,    # GDl thickness [m]
        'MPL_thick': 50e-6,     # MPL thickness [m]
        'CL_thick': 5e-6,       # CL thickness [m]
        'MEM_thick': 20e-6,     # membrane thickness [m]
        'rib_ndiv': 5,          # number of divisions for the rib area
        'chan_ndiv': 12,        # number of divisions for the channel area
        'GDL_ndiv': 13,         # number of divisions over the GDL thickness
        'MPL_ndiv': 10,         # number of divisions over the MPL thickness
        'CL_ndiv': 10           # number of divisions over the CL thickness
        },
    'materials':{
        'GDL_Drel_TP':0.05,      # Relative through plane diffusivity in the GDL [-]
        'GDL_Drel_IP':10.4,      # Relative in plane diffusivity in the GDL [-]
        'MPL_Drel':10.25,        # Relative diffusivity in the MPL [-]
        'CL_Drel':10.25,         # Relative diffusivity in the CL [-]
        'MPL_Dknud':5e-5,       # Knudsen diffusivity in the MPL [m^2/s]
        'CL_Dknud':1e-5,        # Knudsen diffusivity in the CL [m^2/s]
        'CL_Rfilm':500,        # O2 transport resistance of the ionomer film [s/m]
        'CL_SAD':20e6,          # Surface area density in 1/m (m^2/m^3)
        'MEM_cond':10,           # Membrane ionic conductivity [1/(Ohm*m)]
        'CL_cond':50,            # CL effective ionic conductivity [1/(Ohm*m)]
        'CL_i0': 5e-5,          # exchange current density per Pt surface [A/m^2] at c0
        'Elec_res': 1e-6,       # total electrical resistivity [Ohm*m^2]
        'GDL_porosity':0.8,     # GDl porosity [-]
        'MPL_porosity':0.7,     # MPL porosity [-]
        'CL_porosity':0.55,     # CL porosity [-]
        'Cdl': 1e7,             # Double layer capacitance [F/m^3]
        }
    }

    # empty parameters variable
params_empty = {
    'physics':{},
    'conditions':{},
    'geometry':{},
    'materials':{}
    }

    # default list of current densities for the PGA simulations
i_base_default = np.append(np.geomspace(10, 1000, 20, endpoint=False),
                           np.linspace(1000, 40000, 157))

    # default list of concentrations for the LCA simulations
c_base_default = np.array([0.01, 0.02, 0.03, 0.04, 0.05])

# ---------------------------------------------------------------------------------

def set_def_params(params):
    """
    Sets the default parameters

    Parameters
    ----------
    params : dict
        New default parameters which will be used in all simulations unless
        overriden.

    Returns
    -------
    None.

    """
    
    global def_params
    
    def_params = params
    
# ---------------------------------------------------------------------------------

def params_override(params, **kwargs):
    """
    Overrides values in the list of simulation parameters with the values of
    all other names parameters.

    Parameters
    ----------
    params : dict
        Original simulation parameters
    **kwargs : various
        All other named parammeters (used for overriding)

    Returns
    -------
    New simulation parameters
    """
    
    from copy import deepcopy
    
        # create a deep copy of the parameters
    new_params = deepcopy(params)
    
        # loop for all named parameters
    for key in kwargs.keys():
        found = False
        
            # find the parameters in the sub groups and replace it if found
        for group in new_params.keys():
            if key in new_params[group].keys():
                new_params[group][key] = kwargs[key]
                found = True
                
            # if not found, raise an error
        if not found:
            raise ValueError('\'' + key + '\' is not a valid parameter for the simulation')
                
        # return the new value
    return new_params

# ---------------------------------------------------------------------------------

def calc_zod_matrix(odn, omega=0):
    """
    Computes Zod, the oxygen diffusion impedance matrix. Zod is a 2D matrix
    relating the vector of O2 molar flows at the active nodes, and the
    vector of O2 concentration at the active nodes.
    
    NOTE: in the current implementation, only the steady state is considered
    and the omega parameter is ignored. Future implementations will include
    modeling of EIS, where the Zod matrix will be a complex number matrix
    depending on omega.

    Parameters
    ----------
    odn : dict
        Description of the oxygen diffusion network:
        'nodes': list of node pore volumes [m^3]
        'nact': number of active (catalyst) nodes
        'branches': list of branches between the nodes. Each branch has the
        format (node1, node2, res) where 'res' is an oxygen transport resistance in [s/m^3].
        In the special case where node2 equals -1, the branch is considered to link
        node1 with the supply channel.
            
    omega : double
        Angular frequency [rad/s] (not used yet !)

    Returns
    -------
    zod_matrix : 2D numpy array
        Oxygen diffusion impedance matrix in [s/m^3]

    """
    
    n_tot = odn['nodes'].size # total number of nodes
    n_act = odn['n_act']      # number of active nodes
    
        # create the empty oxygen transport (OTN) network matrix. The OTN
        # matrix relates the vector of the net molar flows at all nodes to the
        # vector of the concentration at all nodes
    if omega == 0:
        otn_matrix = np.zeros([n_tot, n_tot], dtype=np.float64)
    else:
        otn_matrix = np.zeros([n_tot, n_tot], dtype=np.complex64)
    
        # loop for all branches in the network description
    for branch in odn['branches']:
        
            # get the branch parameters
        node1 = branch[0]
        node2 = branch[1]
        resistance = branch[2]
        
            # case of a source branch (connected to the gas channel)
        if node2 == -1:
            otn_matrix[node1, node1] = otn_matrix[node1, node1] + 1/resistance
        
            # case of a "normal" branch (connected between two nodes)
        else:
            otn_matrix[node1, node1] = otn_matrix[node1, node1] + 1/resistance
            otn_matrix[node1, node2] = otn_matrix[node1, node2] - 1/resistance
            otn_matrix[node2, node1] = otn_matrix[node2, node1] - 1/resistance
            otn_matrix[node2, node2] = otn_matrix[node2, node2] + 1/resistance
        
        # if not steady state, include the frequency dependent "storage" component
    if omega != 0:
        for i, vol in enumerate(odn['nodes']):
            otn_matrix[i,i] = otn_matrix[i,i] + omega*complex(0,1)*vol
        # computation of the oxygen diffusion impedance matrix by inverting the
        # OTN matrix and keeping only the part relevant to the active nodes
    zod_matrix = np.linalg.inv(otn_matrix)[0:n_act, 0:n_act]
    
        # compute the inverse of the Zod matrix
    zod_inv = np.linalg.inv(zod_matrix)
            
        # return the matrix and its inverse
    return zod_matrix, zod_inv

# ---------------------------------------------------------------------------------

def calc_rohm_matrix_2d_rib_chan(geometry, materials):
    """
    Computes Rohm, the ohmic resistance matrix. Rohm is a 2D matrix
    relating the vector of current densities at the active nodes, and the
    vector of ohmic overpotentials at the active nodes.

    Parameters
    ----------
    geometry : dict
        Parameters describing the cell geometry (see the default values at the
        beginning of this file for the description of each parameter)
    materials : dict
        Parameters describing the materials property (see the default values at the
        beginning of this file for the description of each parameter)

    Returns
    -------
    Rohm : 2D numpy array
        Ohmic resistance matrix having the dimension NxN where N is the number
        of active nodes.

    """
    
        # get the size parameters
    rib_width = geometry['rib_width']
    chan_width = geometry['chan_width']
    MEM_thick = geometry['MEM_thick']
    CL_thick = geometry['CL_thick']
    
        # get the number of divisions
    rib_ndiv = geometry['rib_ndiv']
    chan_ndiv = geometry['chan_ndiv']
    x_ndiv = rib_ndiv + chan_ndiv
    CL_ndiv = geometry['CL_ndiv']
    
        # get the materials properties
    MEM_cond = materials['MEM_cond']  # Membrane ionic conductivity in 1/(Ohm*m)
    CL_cond = materials['CL_cond']  # CL conductivity in 1/(Ohm*m)
    Elec_res = materials['Elec_res'] # electrical resistivity
    
        # compute the cell sizes
    rib_dx = rib_width/2/rib_ndiv
    chan_dx = chan_width/2/chan_ndiv
    CL_dy = CL_thick/CL_ndiv
    
        # compute the ohmic resistances of the single branches (the rib and
        # channel regions may have different resistance for example if the
        # cell sizes are not the same)
    Rohm_MEM_rib = MEM_thick/(MEM_cond*rib_dx) + Elec_res/rib_dx
    Rohm_MEM_chan = MEM_thick/(MEM_cond*chan_dx) + Elec_res/chan_dx
    Rohm_CL_rib = CL_dy/(CL_cond*rib_dx)
    Rohm_CL_chan = CL_dy/(CL_cond*chan_dx)
    
        # compute the number of nodes
    n_CL = (rib_ndiv+chan_ndiv)*CL_ndiv
    
        # create the empty matrix
    Rohm = np.zeros([n_CL, n_CL])
    
        # loop for all divisions across the channel/rib structure
    for x in range(x_ndiv):
        
            # select the rib or channel resistance values
        Rohm_mem = Rohm_MEM_rib if x < rib_ndiv else Rohm_MEM_chan
        Rohm_CL = Rohm_CL_rib if x < rib_ndiv else Rohm_CL_chan
        
            # create the sub-matrix corresponding to one division in x direction
        sub_matrix = np.zeros([CL_ndiv, CL_ndiv])
        
            # compute the elements of the sub matrix
        for i in range(CL_ndiv):
            for j in range(CL_ndiv):
                sub_matrix[i,j] = Rohm_mem + (min(i,j)+0.5)*Rohm_CL
        
            # insert the sub-matrix in the general Rohm matrix
        Rohm[x*CL_ndiv:(x+1)*CL_ndiv, x*CL_ndiv:(x+1)*CL_ndiv] = sub_matrix
        
    return Rohm

# ---------------------------------------------------------------------------------
    
def odn_2d_rib_chan(geometry, materials, conditions):
    """
    This function generates the description of an oxygen diffusion network,
    in the form of a list of nodes having a given volume and a list of branches
    having a given oxygen transport resistance and linking either two nodes
    or one node and the gas supply channel ("source node").

    Parameters
    ----------
    geometry : dict
        Parameters describing the cell geometry (see the default values at the
        beginning of this file for the description of each parameter)
    materials : dict
        Parameters describing the materials property (see the default values at the
        beginning of this file for the description of each parameter)

    Returns
    -------
    n : integer
        Number of active nodes.
    n_tot : integer
        Total number of nodes.
    odn : array of 3-elements tuples
        List of network branches. Each branch is a tuple of 3 elements:
            - Index of first node
            - Index of second node (value of -1 means the source node)
            - Branch resistance
    """
    
        # get the size parameters
    rib_width = geometry['rib_width']
    chan_width = geometry['chan_width']
    GDL_thick = geometry['GDL_thick']
    MPL_thick = geometry['MPL_thick']
    CL_thick = geometry['CL_thick']
    
        # get the number of divisions
    rib_ndiv = geometry['rib_ndiv']
    chan_ndiv = geometry['chan_ndiv']
    x_ndiv = rib_ndiv + chan_ndiv
    GDL_ndiv = geometry['GDL_ndiv']
    MPL_ndiv = geometry['MPL_ndiv']
    CL_ndiv = geometry['CL_ndiv']
    
        # get the effective diffusion values
    GDL_Deff_TP = conditions['D_O2']*materials['GDL_Drel_TP']  # Effective through plane GDL diffusivity [m^2/s]
    GDL_Deff_IP = conditions['D_O2']*materials['GDL_Drel_IP']  # Effective in plane GDL diffusivity [m^2/s]
    MPL_Deff_TP = 1/(1/(conditions['D_O2']*materials['MPL_Drel']) +
                     1/(materials['MPL_Dknud']))               # Effective through plane MPL diffusivity [m^2/s]
    MPL_Deff_IP = MPL_Deff_TP                                  # Effective in plane MPL diffusivity [m^2/s]
    CL_Deff = 1/(1/(conditions['D_O2']*materials['CL_Drel']) +
                 1/(materials['CL_Dknud']))                    # Effective CL diffusivity [m^2/s]
    CL_Rfilm = materials['CL_Rfilm']                           # O2 transport resistance in s/m
    CL_SAD = materials['CL_SAD']                               # Surface area density in 1/m (m^2/m^3)
    
        # get the materials porosities
    GDL_porosity = materials['GDL_porosity']
    MPL_porosity = materials['MPL_porosity']
    CL_porosity = materials['CL_porosity']
    
        # compute the cell sizes
    rib_dx = rib_width/2/rib_ndiv
    chan_dx = chan_width/2/chan_ndiv
    GDL_dy = GDL_thick/GDL_ndiv
    MPL_dy = MPL_thick/MPL_ndiv
    CL_dy = CL_thick/CL_ndiv
    
        # compute the volumes of the active nodes
    vol_rib_nodes = rib_dx*CL_dy
    vol_chan_nodes = chan_dx*CL_dy
    vols = np.append(np.ones(rib_ndiv*CL_ndiv)*vol_rib_nodes,
                     np.ones(chan_ndiv*CL_ndiv)*vol_chan_nodes)
    
        # compute the transport resistances
    R_GDL_TP_rib = GDL_dy/(GDL_Deff_TP*rib_dx)
    R_GDL_TP_chan = GDL_dy/(GDL_Deff_TP*chan_dx)
    R_GDL_IP_rib = rib_dx/(GDL_Deff_IP*GDL_dy)
    R_GDL_IP_chan = chan_dx/(GDL_Deff_IP*GDL_dy)
    R_MPL_TP_rib = MPL_dy/(MPL_Deff_TP*rib_dx)
    R_MPL_TP_chan = MPL_dy/(MPL_Deff_TP*chan_dx)
    R_MPL_IP_rib = rib_dx/(MPL_Deff_IP*MPL_dy)
    R_MPL_IP_chan = chan_dx/(MPL_Deff_IP*MPL_dy)
    R_CL_TP_rib = CL_dy/(CL_Deff*rib_dx)
    R_CL_TP_chan = CL_dy/(CL_Deff*chan_dx)
    R_CL_FILM_rib = CL_Rfilm/CL_SAD/(CL_dy*rib_dx)
    R_CL_FILM_chan = CL_Rfilm/CL_SAD/(CL_dy*chan_dx)
    
        # compute the number of nodes per region
    n_GDL = (rib_ndiv+chan_ndiv)*GDL_ndiv
    n_MPL = (rib_ndiv+chan_ndiv)*MPL_ndiv
    n_CL = (rib_ndiv+chan_ndiv)*CL_ndiv
    
        # number of nodes
    n_act = n_CL                   # number of active nodes (= number of nodes in CL region)
    n_int = n_GDL + n_MPL + n_CL    # number of internal nodes
    n_tot = n_act + n_int           # total number of nodes
    
        # index offset of the internal nodes in the network matrix
    GDL_in_off = n_CL
    MPL_in_off = n_CL + n_GDL
    CL_in_off = n_CL + n_GDL + n_MPL
    
        # start with empty network
    branches = []
    nodes = np.zeros(n_tot)
        
        # compute the node "volumes" (= areas because of the 2d geometry)
    for x in range(x_ndiv):
        
            # compute the node width for the particular region
        dx = rib_dx if x < rib_ndiv else chan_dx
        
            # compute the GDL node volumes
        for y in range(GDL_ndiv):
            nodes[GDL_in_off+y*x_ndiv+x] = GDL_dy*dx*GDL_porosity
        
            # compute the MPL node volumes
        for y in range(MPL_ndiv):
            nodes[MPL_in_off+y*x_ndiv+x] = MPL_dy*dx*MPL_porosity
        
            # compute the CL node volumes
        for y in range(CL_ndiv):
            nodes[CL_in_off+y*x_ndiv+x] = CL_dy*dx*CL_porosity
            
    
        # through-plane branches of the channel-GDL interface
    for x in range(rib_ndiv, x_ndiv):
        branches.append((GDL_in_off+x, -1, R_GDL_TP_chan/2))
        
        # loop for all divisions in the rib/channel direction
    for x in range(x_ndiv):
        
            # selection of through-plane resistances according
            # to the position in rib_channel structure
        if x < rib_ndiv:
                # rib region
            R_GDL_TP = R_GDL_TP_rib
            R_MPL_TP = R_MPL_TP_rib
            R_CL_TP = R_CL_TP_rib
            R_CL_FILM = R_CL_FILM_rib
            
        else:
                # channel region
            R_GDL_TP = R_GDL_TP_chan
            R_MPL_TP = R_MPL_TP_chan
            R_CL_TP = R_CL_TP_chan
            R_CL_FILM = R_CL_FILM_chan
            
            # selection of in-plane resistances according
            # to the position in rib_channel structure
        if x < rib_ndiv-1:
                # rib region
            R_GDL_IP = R_GDL_IP_rib
            R_MPL_IP = R_MPL_IP_rib
        elif x == rib_ndiv-1:
                # rib-channel interface
            R_GDL_IP = (R_GDL_IP_rib+R_GDL_IP_chan)/2
            R_MPL_IP = (R_MPL_IP_rib+R_MPL_IP_chan)/2
        else:
                # channel region
            R_GDL_IP = R_GDL_IP_chan
            R_MPL_IP = R_MPL_IP_chan
            
            # through-plane branches inside the GDL
        for y in range(GDL_ndiv-1):
            branches.append((GDL_in_off+y*x_ndiv+x, GDL_in_off+(y+1)*x_ndiv+x, R_GDL_TP))
    
            # through-plane branches of the GDL-MPL interface
        branches.append((MPL_in_off-x_ndiv+x, MPL_in_off+x, (R_GDL_TP+R_MPL_TP)/2))
    
            # through-plane branches inside the MPL
        for y in range(MPL_ndiv-1):
            branches.append((MPL_in_off+y*x_ndiv+x, MPL_in_off+(y+1)*x_ndiv+x, R_MPL_TP))
    
            # through-plane branches of the MPL-CL interface
        branches.append((CL_in_off-x_ndiv+x, CL_in_off+x, (R_MPL_TP+R_CL_TP)/2))
    
            # through-plane branches inside the CL
        for y in range(CL_ndiv-1):
            branches.append((CL_in_off+y*x_ndiv+x, CL_in_off+(y+1)*x_ndiv+x, R_CL_TP))
            
        if x < x_ndiv-1:
            
                # in-plane branches in the GDL
            for y in range(GDL_ndiv):
                branches.append((GDL_in_off+y*x_ndiv+x, GDL_in_off+y*x_ndiv+x+1, R_GDL_IP))
                
                # in-plane branches in the MPL
            for y in range(MPL_ndiv):
                branches.append((MPL_in_off+y*x_ndiv+x, MPL_in_off+y*x_ndiv+x+1, R_MPL_IP))
        
            # film resistance branches inside the CL
        for y in range(CL_ndiv):
            branches.append((CL_in_off+y*x_ndiv+x, x*CL_ndiv+CL_ndiv-y-1, R_CL_FILM*(x+1)/4))
        
        # return the computed oxygen diffusion network
    return {'n_act':n_act, 'nodes':nodes, 'branches':branches, 'vols':vols}

# ---------------------------------------------------------------------------------

def calc_i(heta, Rohm, Zod_mat, params, i0, i_init=None):
    """
    Computes the vector of node currents and the vector of O2 concentrations
    for a given total overpotential value. The curve_fit() function from the
    "scipy" package is used for fitting, with the default solver (LM algorithm).
    
    The function solves first for the distribution of ln(c) instead of directly
    solving for the vector of node currents, which helps convergence without
    running into illegal values when approaching limiting currents (localy
    or globaly).

    Parameters
    ----------
    heta : double
        Target value of the total overpotential.
    Rohm : 2D numpy array
        Ohmic resistance matrix.
    Zod_mat : tuple (2D numpy array, 2D numpy array)
        Oxygen diffusion impedance matrix and its inverse (both are passed
        as parameters so that the latter is not recomputed each time).
    params : dict
        Simulation parameters (see the default values at the top of this file
        for details).
    i0 : 1D numpy array
        Vector of exchange currents at the active nodes. Units is [A], representing
        the total current for 1 node.
    i_init : 1D numpy array, optional
        Initial guess for the vector of node currents. The default is None.

    Returns
    -------
    i_fit: 1D numpy array
        Array of node currents in [A]
    c_fit: 1D numpy array
        Array of O2 concentrations at the nodes [mol/m^3]
    """
    
    from scipy.optimize import curve_fit
    import warnings
    
        # get the necessary simulations parameters
    conditions = params['conditions']
    physics = params['physics']
    
    cref = conditions['cref']       # reference O2 concentration
    T = conditions['T']             # temperature
    alpha = conditions['alpha']     # transfer coefficient
    gamma = conditions['gamma']     # kinetic order of reaction
    
    R = physics['R']                # gas constant
    F = physics['F']                # Faraday constant
    z = physics['z']                # electrons count
    
        # compute the Tafel slope (ln basis)
    b = R*T/(alpha*F) 

        # compute the concentration overpotential corresponding to a
        # decrease of concentration of a factor e           
    k = (gamma/alpha + 1/z)*R*T/F   
        
        # extract the Zod matrix and its inverse
    Zod = Zod_mat[0]
    Zod_inv = Zod_mat[1]
    
    # --- v_heta: internal function used by the solver, returns the array of
    # total overpotential at each node as a function of the array of ln(c)
    
    def v_heta(dummy, *vals):
        
        warnings.filterwarnings('ignore')
            
            # get the ln(c) array as a numpy vector
        x = np.array(vals)
        
            # get the vector size
        n = x.size
        
            # compute the ohmic overpotential distribution
        heta_ohm = np.dot(np.dot(Rohm, z*F*Zod_inv), cref*np.ones(n) - np.exp(x))
        
            # compute the charge transfer overpotential distribution
        heta_ct = b*(np.log(np.dot(z*F*Zod_inv, cref*np.ones(n) - np.exp(x))) - np.log(i0))
        
            # compute the concentration overpotential distribution
        heta_conc = k*(np.log(cref)*np.ones(n) - x)
        
            # compute the total overpotential as the sum of the three contributions
        heta_tot = heta_ohm + heta_ct + heta_conc
        
            # return the result
        return heta_tot
    
    # --- j_matrix: internal function used by the solver, returns the Jacobian
    # matrix (matrix of partial derivatives of heta versus ln(c) for each
    # possible combination of nodes)
    
    def j_matrix(dummy, *vals):
        
            # get the ln(c) array as a numpy vector
        x = np.array(vals)
        
            # get the vector size
        n = x.size
        
            # get the node concentrations vector
        c = np.exp(x)
        
            # get the node currents vector
        i = np.dot(z*F*Zod_inv, (cref - c))
        
            # create a diagonal matrix with the node concentrations
        c_mat = np.diag(c)
        
            # create a diagonal matrix with the inverses of the node currents
        i_mat_inv = np.diag(1/i)
        
            # compute the Jacobian matrix
        j_mat = -np.dot(np.dot((Rohm + b*i_mat_inv), z*F*Zod_inv), c_mat)-k*np.eye(n)
        
            # return the value
        return j_mat
    
    # --- main body of the function
    
        # get the number of nodes
    n = Rohm.shape[0]
    
        # if no intial guess is given, use a concentration distribution
        # slightly lower (0.001%) than the reference concentration and compute
        # the corresponding ln(c) vector
    if i_init is None:
        x_init = np.ones(n)*np.log(cref*(1-1e-5))
        
        # if an initial guess is given for the vector of nodes current, compute
        # the corresponding ln(c) vector
    else:
        x_init = np.log(cref - np.dot(Zod/z/F, i_init))
        
        # create a target vector for the total overpotential (same value for
        # all nodes)
    heta_target = heta*np.ones(n)
    
        # solve for the ln(c) vector corresponding to the specified total
        # overpotential. 
    x_fit = curve_fit(v_heta, np.zeros(n), heta_target, x_init, jac=j_matrix)
    
        # compute the vector of node currents corresponding to the computed
        # ln(c) vector
    i_fit = np.dot(z*F*Zod_inv, (cref - np.exp(x_fit[0])))
    
        # compute the vector of O2 concentrations
    c_fit = np.exp(x_fit[0])
    
        # return the results
    return i_fit, c_fit
    
#---------------------------------------------------------------------------------

def calc_iv(h_range, Rohm, Zod, vols, params, i0, descr=None):
    """
    Computes a full IV curve for the given list of overpotential values

    Parameters
    ----------
    h_range : 1D numpy array
        Array of overpotential values
    Rohm : 2D numpy array
        Ohmic resistance matrix
    Zod : 2D numpy array
        Oxygen diffusion impedance matrix
    vols : 1D numpy array
        Array of node total volumes
    params : dict
        Simulation parameters (see the default values at the top of this file
        for details).
    i0 : 1D numpy array
        Array of node exchange currents (in [A] per node)
    descr : string, optional
        String to be displayed in the progress bar. The default is None.

    Returns
    -------
    i_vals : array of 1D numpy arrays
        Each element contains the current density distribution in [A/m^3].
    c_vals : array of 1D numpy arrays
        Each element contains the concentration distribution in [mol/m^3].

    """
    
    from tqdm import tqdm
    
        # variable for keeping the last obtained current distribution
        # which will be used as initial guess for the next point
    last_i = None
    
        # initialize empty result arrays
    i_vals = []
    c_vals = []
    
        # If not specified, use the default description for the progress bar
    if descr is None:
        desc_string = 'Computing IV'
    else:
        desc_string = descr
        
        # if the description string is 'silent', don't output the progress
    it = h_range if desc_string == 'silent' else tqdm(h_range, desc_string)
    
        # loop for all overpotential values
    for heta in it:
        
            # compute the node currents and concentrations
        i_val, c_val = calc_i(heta, Rohm, Zod, params, i0, i_init=last_i)
        
            # keep the node currents as initial guess for the next iteration
        last_i = i_val
        
            # store the node currents divided by the node volumes (to get
            # the volumetric current densities in the results array)
        i_vals.append(i_val/vols)
        
            # store the node concentrations in the results array
        c_vals.append(c_val)
        
        # return the results
    return i_vals, c_vals
    
#---------------------------------------------------------------------------------

def dp2press(dp):
    """
    Utiliy function used for computing the H2O partial pressure from the
    dew point

    Parameters
    ----------
    dp : double
        Dew point in [K].

    Returns
    -------
    double
        H2O partial pressure in [Pa].

    """
    
        # compute the dew point in [°C]
    dp_C = dp - 273.15
    
        # compute the log of the partial pressure with a poynomial function fitted
        # on tabulated data from E.W. Lemmon "Vapor pressure and other saturation 
        # properties of water" in CRC Handbook of Chemistry and Physics, 90th edition 
        # (2009-2010). The polynomial fitting is done on the data in the range 0-100°C
    Log_p = -0.000000000815736*dp_C**4 \
        +0.000000423586*dp_C**3 \
        - 0.000127489*dp_C**2 \
        + 0.0315264*dp_C \
        + 2.78628
        
        # compute the partial pressure
    p_H2O = 10**Log_p
        
        # return the value
    return p_H2O

#---------------------------------------------------------------------------------
        
def interp_loc_data(loc_data, u_vals, u_val):
    """
    Utility function to interpolate the local data

    Parameters
    ----------
    loc_data : dict
        Computed current density and concentration distribution for a series of
        voltages
    u_vals: double
        list of voltage values for which the local data was computed
    u_val : double
        Cell voltage value for which the local data has to be interpolated.

    Returns
    -------
    i_array: 2D numpy array
        Interpolated current density distribution data
    c_array: 2D numpy array
        Interpolated concentration data

    """
    
        # if the target voltage value is invalid or out of range, return NaN values
    if np.isnan(u_val) or u_val < np.min(u_vals) or u_val > np.max(u_vals):
        ld_shape = loc_data['i_loc'][0].shape
        nan_array = np.zeros(ld_shape)
        nan_array[:] = np.nan
        return nan_array, nan_array
    
        # otherwise, compute the results by linear interpolation
    else:
        i1 = np.max(np.where(u_vals >= u_val))
        frac = (u_val-u_vals[i1])/(u_vals[i1+1]-u_vals[i1])
        i_array = loc_data['i_loc'][i1] + frac*(loc_data['i_loc'][i1+1]-loc_data['i_loc'][i1])
        c_array = loc_data['c_loc'][i1] + frac*(loc_data['c_loc'][i1+1]-loc_data['c_loc'][i1])
        return i_array, c_array
    
#---------------------------------------------------------------------------------
    
def get_iv_2d(i_base=None, descr=None, eis_freqs=None, **kwargs):
    """
    Compute an IV curve for a cell defined by a 2D geometry. If a list of
    current densities is specified, the IV curve is computed for these current
    densities. Otherwise, the IV curve is computed for fixed array of cell voltages.

    Parameters
    ----------
    i_base : 1D numpy array, optional
        List of average current densities. The default is None.
    descr : string, optional
        Description used for the progress bar. The default is None.
    **kwargs : dict
        All othe named parameters are used to override the default parameters.

    Returns
    -------
    i_iv : 1D numpy array
        average current densities of the computed IV curve.
    u_iv : 1D numpy array
        cell voltages of the computed IV curve.
    loc_data : dict
        Local data (distributiom of current density and concentration). 
        Contains 3 elements:
            'u': voltage values for which the local data is given
            'i_vals': current density distributions for each of the u values
            'c_vals': concentration distribution for each of the u values
    """
    
    from scipy.interpolate import interp1d
    from tqdm import tqdm
    from numpy.linalg import inv
    
        # get the default parameters and override them with the specific parameters
    params = params_override(def_params, **kwargs)
    
        # get the different parameter groups
    phys = params['physics']
    cond = params['conditions']
    geom = params['geometry']
    mat = params['materials']
    
        # get specific parameter values
    R = phys['R']   # gas constant
    F = phys['F']   # faraday constantg
    z = phys['z']   # number of electrons
    D_O2_N2_1bar = phys['D_O2_N2_1bar']     # O2 diffusivity in N2 at 1 bar
    D_O2_He_1bar = phys['D_O2_He_1bar']     # O2 diffusivity in He at 1 bar
    c0 = phys['c0'] # O2 concentration in normal conditions (1.01325 bar, 0°C) [mol/m^3]
    
    i0 = mat['CL_i0']   # exchange current density per area of Pt
    
        # compute the O2 diffusivity as a function of the pressure and carrier gas
        # if the O2 concentration is 1.0, use a very high diffusivity value (1 m^2/s)
        # because the transport is by convection, not diffusion
    cond['D_O2'] = 1e5/cond['p']*(D_O2_He_1bar if cond['carrier'] == 'He' else D_O2_N2_1bar) \
        if cond['x_O2'] < 1.0 else 1.0
            
        # compute the H2O partial pressure
    cond['p_H2O'] = cond['RH']*dp2press(cond['T'])
    
        # compute the reference O2 partial pressure
    cond['p_O2'] = (cond['p']-cond['p_H2O'])*cond['x_O2']
    
        # compute the reference O2 concentration
    cond['cref'] = cond['p_O2']/(R*cond['T'])
    
        # compute the equilibrium voltage corresponding to the reference O2 concentration
        # (based on an equilibrium voltage of 1.2V when the O2 molar fraction is 1.0
        # because I was too lazy to put the full calculation of E0)
    u0 = 1.2 + R*cond['T']/(z*F)*np.log(cond['x_O2'])
    
    alpha = cond['alpha']       # transfer coefficient
    gamma = cond['gamma']       # kinetic order of reaction
    T = cond['T']               # temperature
    
        # get the minimum and maximum values of total overpotential
    heta_min = cond['heta_min']
    heta_max = cond['heta_max']
    
        # create a range of overpotential values with 100 points
    heta_range = np.linspace(heta_min, heta_max, 100)
    
        # compute the corresponding cell voltages
    u_vals_calc = u0-np.array(heta_range)
    
        # compute the ohmic resistance matrix based on the geometry and materials 
    Rohm = calc_rohm_matrix_2d_rib_chan(geom, mat)
    
        # compute the oxygen diffusion network based on the geometry, materials and conditions
    odn = odn_2d_rib_chan(geom, mat, cond)
    
        # create the oxygen diffusion impedance matrix based on the previously calculated network
    Zod = calc_zod_matrix(odn, omega=0)
    
        # compute the vector of node exchange currents
    i0_nodes = i0*mat['CL_SAD']*odn['vols']*(cond['cref']/c0)**cond['gamma']
    
        # create the parameters set for the simulation
    p = {'physics':phys, 'conditions':cond, 'geometry':geom, 'materials':mat}
    
        # perform the simulation
    i_vals, c_vals = calc_iv(heta_range, Rohm, Zod, odn['vols'], p, i0=i0_nodes, descr=descr)
    
        # get the shape of the results
    res_shape = [geom['rib_ndiv']+geom['chan_ndiv'], geom['CL_ndiv']]
    n_nodes = (geom['rib_ndiv']+geom['chan_ndiv'])*geom['CL_ndiv']
    
        # reshape the current density and concentration values based on the 2D shape
    i_vals = [np.reshape(x, res_shape) for x in i_vals]
    c_vals = [np.reshape(x, res_shape) for x in c_vals]
    loc_data = {'i_loc':i_vals, 'c_loc':c_vals}
    
        # compute the average current density
        # !!! TODO: improve this computation. currently, it assumes that all
        # nodes have the same volumes, and will give wrong results if it is not
        # the case !!!
    i_avg = [np.mean(x)*geom['CL_thick'] for x in i_vals]
    
        # if a list of current densities is specified, compute the IV curve
        # for these current densities using linear interpolation
    if i_base is not None:
        int_func = interp1d(i_avg, u_vals_calc, kind='linear', bounds_error=False)
        u_iv = np.array(int_func(i_base))
        i_iv = np.array(i_base)
        
            # and interpolate the local data
        loc_vals_int = [interp_loc_data(loc_data, u_vals_calc, x) for x in u_iv]
        loc_data = {'i_loc':[x[0] for x in loc_vals_int], 'c_loc':[x[1] for x in loc_vals_int]}
        
        # otherwise, keep the results computed for specific voltage values
    else:
        u_iv = np.array(u_vals_calc)
        i_iv = np.array(i_avg)
        
    eis_data = []
        
        # if EIS needs to be computed
    if eis_freqs is not None:
        
        area = (geom['rib_width']+geom['chan_width'])/2
    
            # compute the list of rotation frequencies
        omega_vals = 2*np.pi*eis_freqs
        
            # compute the Zod matrices for all omega values
        Zod_EIS = [calc_zod_matrix(odn, omega=w)[0] for w in
                   tqdm(omega_vals, desc='EIS: Computing diffusion impedance matrices')]
                      
        #print('Zod_EIS: ', Zod_EIS)
        
        Cdl = mat['Cdl']*odn['vols']
        #print('Cdl: ', np.sum(Cdl))
        Zdl_inv = [(complex(0,1)*w*np.diag(Cdl)) for w in omega_vals]
        
        #for i in range(len(omega_vals)):
        #    print('omega = {:.3g} / Zdl = {:.3g}'.format(omega_vals[i], 1/np.sum(Zdl_inv[i])))
        
        
        for i in tqdm(range(len(i_iv)), desc='EIS: Computing spectra'):
            
            i_loc = np.reshape(loc_data['i_loc'][i], [n_nodes])
            c_loc = np.reshape(loc_data['c_loc'][i], [n_nodes])
            
            k = (gamma/alpha + 1/z)*R*T/F
            b = R*T/(alpha*F)
            
            Zct = b*inv(np.diag(i_loc*odn['vols']))
                          
            #print('Zct: ', 1/np.sum(inv(Zct)))
        
            Ztot = np.zeros([len(omega_vals)], dtype=complex)
            
            for i in range(len(omega_vals)):
                
                Zc = k/(z*F)*np.dot(inv(np.diag(c_loc)), Zod_EIS[i])
                          
                #print('Zc: ', np.sum(Zc))
                
                #break
                
                Ztot[i] = 1/np.sum(inv(Rohm + inv(inv(Zct + Zc) + Zdl_inv[i])))*area
                
            eis_data.append(Ztot*1e4)
            
            #break
    
        # return the results
    return i_iv, u_iv, loc_data, eis_data
    
#---------------------------------------------------------------------------------
    
def sim_pga(i_base=i_base_default, show_plots=True, **kwargs):
    """
    Simulation of the pulsed gas analysis (PGA) experiment. The function shows
    4 plots summarizing the results and does not return any data.
    TODO: modify the function to optionally return data, so that it can be used
    for fitting and/or in the fram of parametric studies

    Parameters
    ----------
    i_base : 1D numpy array, optional
        List of current densities for the IV curves. The default is i_base_default.
    show_plots : boolean, optional
        Display the results as plots. The default is True.
    **kwargs : dict
        All other named parameters are used to override the default simulation
        parameter values.

    Returns
    -------
    None.

    """
    
    import matplotlib.pyplot as plt
    
        # get the default parameters overriden with the specific parameters
    params = params_override(def_params, **kwargs)
    
        # get a parameter set for the Helox IV curve
    kwargs_he = {**kwargs, **{'carrier':'He'}}
    
        # get a parameter set for the pure O2 IV curve
    kwargs_o2 = {**kwargs, **{'carrier':'He', 'x_O2':1.0}}
    
        # perform the simulation for the 3 curves (air, Helox and pure O2)
    _, u_air, ld_air, _ =  get_iv_2d(i_base, descr='Computing Air IV', **kwargs)
    _, u_he, _, _  = get_iv_2d(i_base, descr='Computing Helox IV', **kwargs_he)
    _, u_o2, _, _  = get_iv_2d(i_base, descr='Computing O2 IV', **kwargs_o2)
    
        # get the necessary physical parameters
    R = params['physics']['R']              # gas constant
    F = params['physics']['F']              # Faraday constant
    z = params['physics']['z']              # number of electrons
    T = params['conditions']['T']           # Temperature
    alpha = params['conditions']['alpha']   # transfer coefficient
    gamma = params['conditions']['gamma']   # kinetic order of reaction
    x_O2 = params['conditions']['x_O2']     # O2 molar fraction
    
        # compute the kinetic and thermodynamic improvement for pure O2
    delta_u_o2 = -(R*T)/F*(gamma/alpha+1/z)*np.log(x_O2)
    
        # compute the bulk diffusion losses (Helox-air difference)
    mtl_bulk = (u_he-u_air)*1000
    
        # compute the Knudsen+film diffusion losses (O2-Helox difference
        # corrected for the kinetic/thermodynamic improvement)
    mtl_nb = (u_o2-u_he-delta_u_o2)*1000
    
        # get the voltage value at 2 A/cm^2
    u_2A = u_air[np.abs(i_base-20000).argmin()]
    
        # get the local data at 2 A/cm^2
    iloc_2A = ld_air['i_loc'][np.abs(i_base-20000).argmin()]
    cloc_2A = ld_air['c_loc'][np.abs(i_base-20000).argmin()]
    
        # if out of range (meaning 2A/cm^2 was not reached), use NaN values
    if np.isnan(u_2A):
        ld_shape = ld_air['i_loc'][0].shape
        iloc_2A = np.zeros(ld_shape)
        iloc_2A[:] = np.nan
        cloc_2A = iloc_2A
    
        # if the results display is activated
    if show_plots:
        
            # create a figure with 4 subplots
        fig, ax = plt.subplots(2,2,figsize=(15,10))
        plot1 = ax[0,0]
        plot2 = ax[0,1]
        plot3 = ax[1,0]
        plot4 = ax[1,1]
        
            # plot 1: IV curves for air, Helox and pure O2
        plot1.plot(i_base/10000, u_air)
        plot1.plot(i_base/10000, u_he)
        plot1.plot(i_base/10000, u_o2)
        plot1.legend(['Air', 'Helox', 'O2'])
        plot1.set_xlabel('Current density [A/cm^2]')
        plot1.set_ylabel('Cell voltage [V]')
        plot1.set_title('IV Curves')
        
            # plot 2: mass transport loses as a function of current density
        plot2.plot(i_base/10000, mtl_bulk)
        plot2.plot(i_base/10000, mtl_nb)
        plot2.legend(['Bulk diffusion', 'Knudsen+film'])
        plot2.set_xlabel('Current density [A/cm^2]')
        plot2.set_ylabel('Mass treansport losses [mV]')
        plot2.set_title('Mass transport losses')
        
            # compute the x and y scales for the distribution plots
        xmax = params['geometry']['CL_thick']*1e6
        ymax = (params['geometry']['rib_width'] + params['geometry']['chan_width'])*1e3/2
        nx = params['geometry']['CL_ndiv']
        ny = params['geometry']['rib_ndiv'] + params['geometry']['chan_ndiv']
        dx = xmax/nx
        dy = ymax/ny
        
            # plot 3: current density distribution at 2A/cm^2
        cs = plot3.contour(dx*(0.5+np.arange(nx)),dy*(0.5+np.arange(ny)),iloc_2A/1e9)
        plot3.set_xlim([0,xmax])
        plot3.set_ylim([0,ymax])
        plot3.set_xlabel('Position across CL [um]')
        plot3.set_ylabel('Position across rib-chan structure [mm]')
        plot3.set_title('Current density distribution at 2A/cm^2 [A/mm^3]')
        plot3.clabel(cs)
        
            # plot 4: concentration distribution at 2A/cm^2
        cs = plot4.contour(dx*(0.5+np.arange(nx)),dy*(0.5+np.arange(ny)),cloc_2A)
        plot4.set_xlim([0,xmax])
        plot4.set_ylim([0,ymax])
        plot4.set_xlabel('Position across CL [um]')
        plot4.set_ylabel('Position across rib-chan structure [mm]')
        plot4.set_title('O2 concentration distribution at 2A/cm^2 [mol/m^3]')
        plot4.clabel(cs)
        
            # show the results
        plt.tight_layout(pad=3.0)
        plt.show()
    
#---------------------------------------------------------------------------------
        
def sim_lca(c_base=c_base_default, show_plots=True, **kwargs):
    """
    Simulation of the limiting current analysis (LCA) experiment. The function shows
    6 plots summarizing the results and does not return any data.
    TODO: modify the function to optionally return data, so that it can be used
    for fitting and/or in the fram of parametric studies

    Parameters
    ----------
    c_base : 1D numpy array, optional
        List of O2 concentrations. The default is c_base_default.
    show_plots : boolean, optional
        Display the results as plots. The default is True.
    **kwargs : dict
        All other named parameters are used to override the default simulation
        parameter values.

    Returns
    -------
    None.

    """
    
    import matplotlib.pyplot as plt
    
    
        # get the default parameters overriden with the specific parameters
    params = params_override(def_params, **kwargs)
    
        # arrays containing the results of the simulations
    i_n2_list = []  # current densities under N2
    u_n2_list = []  # voltages under N2
    ld_n2_list = [] # local data under N2
    i_he_list = []  # current densities under He
    u_he_list = []  # voltages under He
    ld_he_list = [] # local data under He
    ilim_n2_list = [] # limiting currents under N2
    ilim_he_list = [] #limiting currents under He
    
        # loop for all concentrations
    for c in c_base:
        
            # get a parameter list for this given concentration under N2 and under He
        kwargs_n2 = {**kwargs, **{'x_O2':c}}
        kwargs_he = {**kwargs, **{'x_O2':c, 'carrier':'He'}}
    
            # perform the simulations
        i_n2, u_n2, ld_n2, _  =  get_iv_2d(descr='Computing Air IV, xO2='+str(c), **kwargs_n2)
        i_he, u_he, ld_he, _  = get_iv_2d(descr='Computing Helox IV, xO2='+str(c), **kwargs_he)
        
            # append the results to the result arrays
        i_n2_list.append(i_n2)
        u_n2_list.append(u_n2)
        ld_n2_list.append(ld_n2)
        i_he_list.append(i_he)
        u_he_list.append(u_he)
        ld_he_list.append(ld_he)
        ilim_n2_list.append(i_n2[-1])
        ilim_he_list.append(i_he[-1])
        
        # get the necessary parameters
    phys = params['physics']
    cond = params['conditions']
    R = phys['R']   # gas constant
    F = phys['F']   # Faraday constant
    z = phys['z']   # number of electrons
    
        # compute the H2O partial pressure
    p_H2O = cond['RH']*dp2press(cond['T'])
    
        # compute the O2 partial pressure
    p_O2 = (cond['p']-p_H2O)
    
        # compute the reference O2 concentration
    cref = p_O2/(R*cond['T'])
    
        # peform a linear fit of the limiting current as a function of the concentration
    fit_n2 = np.polyfit(c_base, ilim_n2_list,1)
    fit_he = np.polyfit(c_base, ilim_he_list,1)
    
        # compute the corresponding oxygen transport resistances
    rO2_n2 = z*F*cref/fit_n2[0]
    rO2_he = z*F*cref/fit_he[0]
        
        # if the results display is activated
    if show_plots:
        
            # create a figure with 6 sub plots
        fig, ax = plt.subplots(3,2,figsize=(15,15))
        plot1 = ax[0,0]
        plot2 = ax[0,1]
        plot3 = ax[1,0]
        plot4 = ax[1,1]
        plot5 = ax[2,0]
        plot6 = ax[2,1]
        
            # plot 1: IV curves under N2
        plot1.set_xlabel('Current density [A/cm^2]')
        plot1.set_ylabel('Cell voltage [V]')
        plot1.set_title('IV Curves ($O_2$ in $N_2$)')
        
            # plot 2: IV curves under He
        plot2.set_xlabel('Current density [A/cm^2]')
        plot2.set_ylabel('Cell voltage [V]')
        plot2.set_title('IV Curves ($O_2$ in He)')
        
            # add the data to plots 1 and 2 for each concentration value
        for i in range(len(c_base)):
            plot1.plot(i_n2_list[i]/1e4, u_n2_list[i])
            plot2.plot(i_he_list[i]/1e4, u_he_list[i])
            
            # add the legend
        legend = ['$x_{O2}$='+str(c) for c in c_base]
        plot1.legend(legend)
        
            # plot 3: limiting current as a function of O2 molar fraction
        plot3.plot(c_base, np.array(ilim_n2_list)/1e4, 'ro')
        plot3.plot(c_base, np.array(ilim_he_list)/1e4, 'bo')
        plot3.legend(['$O_2$ in $N_2$', '$O_2$ in He'])
        plot3.plot(c_base, 1e-4*(fit_n2[0]*c_base + fit_n2[1]), 'r-')
        plot3.plot(c_base, 1e-4*(fit_he[0]*c_base + fit_he[1]), 'b-')
        plot3.set_xlabel('$O_2$ molar fraction [-]')
        plot3.set_ylabel('Limiting current [A/cm^2]')
        plot3.set_title('Limiting current as a function of $O_2$ molar fraction')
        
            # compute the x and y scales for the distribution plots
        xmax = params['geometry']['CL_thick']*1e6
        ymax = (params['geometry']['rib_width'] + params['geometry']['chan_width'])*1e3/2
        nx = params['geometry']['CL_ndiv']
        ny = params['geometry']['rib_ndiv'] + params['geometry']['chan_ndiv']
        dx = xmax/nx
        dy = ymax/ny
        
            # plot 4: O2 concentration at the lowest voltage for the highers o2 molar fraction under He
        cs = plot4.contour(dx*(0.5+np.arange(nx)),dy*(0.5+np.arange(ny)),ld_he_list[-1]['c_loc'][-1])
        plot4.set_xlim([0,xmax])
        plot4.set_ylim([0,ymax])
        plot4.set_xlabel('Position across CL [um]')
        plot4.set_ylabel('Position across rib-chan structure [mm]')
        plot4.set_title('Concentration in limiting current conditions ($O_2$ in He, $x_{O2}$='+str(c_base[-1])+') [mol/m^3]')
        plot4.clabel(cs)
        
            # plot 5: curren density distribution at the lowest voltage for the highers o2 molar fraction under N2
        cs = plot5.contour(dx*(0.5+np.arange(nx)),dy*(0.5+np.arange(ny)),ld_n2_list[-1]['i_loc'][-1]/1e9)
        plot5.set_xlim([0,xmax])
        plot5.set_ylim([0,ymax])
        plot5.set_xlabel('Position across CL [um]')
        plot5.set_ylabel('Position across rib-chan structure [mm]')
        plot5.set_title('Current distribution in limiting current conditions ($O_2$ in $N_2$, $x_{O2}$='+str(c_base[-1])+') [A/mm^3]')
        plot5.clabel(cs)
        
            # plot 5: curren density distribution at the lowest voltage for the highers o2 molar fraction under He
        cs = plot6.contour(dx*(0.5+np.arange(nx)),dy*(0.5+np.arange(ny)),ld_he_list[-1]['i_loc'][-1]/1e9)
        plot6.set_xlim([0,xmax])
        plot6.set_ylim([0,ymax])
        plot6.set_xlabel('Position across CL [um]')
        plot6.set_ylabel('Position across rib-chan structure [mm]')
        plot6.set_title('Current distribution in limiting current conditions ($O_2$ in He, $x_{O2}$='+str(c_base[-1])+') [A/mm^3]')
        plot6.clabel(cs)
        
            # display the plots
        plt.tight_layout(pad=3.0)
        plt.show()
    
        # print the values of computes oxygen transport resistances
    print('Transport resistance (O2 in N2): {:.2f} s/m'.format(rO2_n2))
    print('Transport resistance (O2 in He): {:.2f} s/m'.format(rO2_he))
    
#---------------------------------------------------------------------------------
    
def sim_eis(i_base=i_base_default, eis_freqs=np.geomspace(1,1e5,101), show_plots=True, **kwargs):
    
    """
    Simulation of the pulsed gas analysis (PGA) experiment. The function shows
    4 plots summarizing the results and does not return any data.
    TODO: modify the function to optionally return data, so that it can be used
    for fitting and/or in the fram of parametric studies
    
    Parameters
    ----------
    i_base : 1D numpy array, optional
        List of current densities for the IV curves. The default is i_base_default.
    eis_freqs : list of frequencies for which to compute the EIS. The default is
        from 1 Hz to 100 kHz with 20 point per decade
    show_plots : boolean, optional
        Display the results as plots. The default is True.
    **kwargs : dict
        All other named parameters are used to override the default simulation
        parameter values.
    
    Returns
    -------
    None.
    
    """
    
    import matplotlib.pyplot as plt
    
        # list of current densities for which to compute and show the EIS
    i_show = [5000, 10000, 15000, 20000, 25000, 30000]
    
        # perform the simulations
    i_vals, u_vals, ld_vals, eis_vals = get_iv_2d(i_show, descr='Computing IV curve', eis_freqs=eis_freqs, **kwargs)
    
    
    styles = ['k', 'k--', 'g', 'g--', 'b', 'b--', 'm', 'm--', 'r', 'r--']
    
    """    # get the EIS for the selected current values
    eis_show = [eis_vals[np.abs(i_base-x).argmin()] for x in i_show]"""
    
        # if the results display is activated
    if show_plots:
    
            # compute the plots legend
        legend = ['i = {:.2g} A/cm^2'.format(x/1e4) for x in i_show]
        
            # create a figure with 2 subplots
        fig, ax = plt.subplots(1,2,figsize=(15,5))
        plot1 = ax[0]
        plot2 = ax[1]
        
            # plot 1: EIS with full range
        for i, eis in enumerate(eis_vals):
            plot1.plot(np.real(eis), -np.imag(eis), styles[i])
        
        plot1.legend(legend)
        plot1.set_xlabel('Real part [Ohm*cm^2]')
        plot1.set_ylabel('-Imaginary part [Ohm*cm^2]')
        plot1.set_title('EIS (full range)')
        
            # plot 2: EIS with full range
        for i, eis in enumerate(eis_vals):
            plot2.plot(np.real(eis), -np.imag(eis), styles[i])
        
        plot2.set_xlabel('Real part [Ohm*cm^2]')
        plot2.set_ylabel('-Imaginary part [Ohm*cm^2]')
        plot2.set_title('EIS (detail)')
        
        plt.axis('equal')
        
        plot2.set_xlim((0.05,0.15))
        plot2.set_ylim((0,0.05))
        
            # show the results
        plt.tight_layout(pad=3.0)
        plt.show()
        
            # create a figure with 2 subplots
        fig, ax = plt.subplots(1,2,figsize=(15,5))
        plot3 = ax[0]
        plot4 = ax[1]
        
            # plot 3: real part
        for i, eis in enumerate(eis_vals):
            plot3.semilogx(eis_freqs, np.abs(eis), styles[i])
        
        plot3.set_xlabel('Frequency [Hz]')
        plot3.set_ylabel('Zabs [Ohm*cm^2]')
        plot3.set_title('Absolute value')
        
            # plot 4: imaginary part
        for i, eis in enumerate(eis_vals):
            plot4.semilogx(eis_freqs, np.angle(eis)*180/np.pi, styles[i])
        
        plot4.set_xlabel('Frequency [Hz]')
        plot4.set_ylabel('Phase [°]')
        plot4.set_title('Phase')
        
            # show the results
        plt.tight_layout(pad=3.0)
        plt.show()
    
#---------------------------------------------------------------------------------