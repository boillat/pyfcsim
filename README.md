# PyFCSim

This project contains a series of functions for the simulation of oxygen transport
in polymer electrolyte fuel cells (PEFCs) and its interplay with proton transport
in the membrane and catalyst layer. The simulated geometry is a 2D representation
of a differential fuel cell. The oxygen concentration is assumed to be homogeneous
in the flow channel.

What is included:
-	O2 transport approximated by binary diffusion (Fickâ€™s law), Knudsen diffusion and film diffusion
-	2D geometry with GDL, MPL and CL
-	Proton transport in the membrane and CL
-	Tafel approximation for the kinetics, with fixed parameters (future implementation may include potential dependent parameters)
What is not included:
-	Multicomponent diffusion (e.g. to take into account H2O vapor) and convective transport
-	Any kind of H2O transport (the membrane and CL conductivity have to be entered as parameters)
-	The impact of liquid water
-	Any effects on the anode side
-	Dynamics (EIS simulation planned in a future implementation)

The use of this model only requires a Python environment. The model implementation is contained in the file 'pyfcsim.py'.
Examples of parametric studies are provided as Jupyter notebooks.